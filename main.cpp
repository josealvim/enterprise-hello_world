#include <iostream>

struct IntrusiveLoggerBase {
	virtual ~IntrusiveLoggerBase() {}
	std::string error_message;
};

IntrusiveLoggerBase* instance;

class IntrusiveLoggerSingleton final: public virtual IntrusiveLoggerBase {
  private:
	IntrusiveLoggerSingleton() {}
  public:
  	static IntrusiveLoggerSingleton& get() {
		if (instance == nullptr)
			instance = new IntrusiveLoggerSingleton;
		return dynamic_cast<IntrusiveLoggerSingleton&>(*instance);
	}

	static void finish() {
		delete instance;
	}
};

struct ErrorCode {
	enum {
		NoError,
		ErrorBadLegacyOutputStringStreamPointer,
		ErrorLegacyOutputStringStreamNullDereference,
	} code;
	operator bool () const {return code != NoError;};
};

struct HelloWorldStringBase {
	virtual char const* get_string_data() const = 0;
	virtual ~HelloWorldStringBase() {}
};

class PrintStrategyBase {
  public:
	virtual ~PrintStrategyBase() {}

	ErrorCode setup_strategy() {
		ErrorCode code = __setup_strategy();
		if (code) {
			logger.error_message = "PrintStrategty::setup_strategy fail " + std::to_string((size_t)(this)) + " " + __FILE__ + ":" + std::to_string(__LINE__);
		}
		return code;
	}

	ErrorCode do_print_job(HelloWorldStringBase const & job)  {
		ErrorCode code = __do_print_job(job);
		if (code) {
			logger.error_message = "PrintStrategty::do_print_job fail " + std::to_string((size_t)(this)) + " " + __FILE__ + ":" + std::to_string(__LINE__);
		}
		return code;
	}

  protected:
	PrintStrategyBase(IntrusiveLoggerBase &l): logger(l) {}

	virtual ErrorCode __setup_strategy() = 0;
	virtual ErrorCode __do_print_job(HelloWorldStringBase const &) = 0;
  
  private:
	IntrusiveLoggerBase &logger;
};

class PrintStrategyLegacy: virtual public PrintStrategyBase {
  public:
	PrintStrategyLegacy(): PrintStrategyBase(IntrusiveLoggerSingleton::get()) {}
	
	ErrorCode __setup_strategy() override {
		legacy_output_string_stream_ptr = &std::cout;
		if (legacy_output_string_stream_ptr == nullptr) {
			return ErrorCode{ErrorCode::ErrorBadLegacyOutputStringStreamPointer};
		}
		return ErrorCode{ErrorCode::NoError};
	}

	ErrorCode __do_print_job(HelloWorldStringBase const &hw) override {
		if (legacy_output_string_stream_ptr == nullptr) {
			return ErrorCode{ErrorCode::ErrorLegacyOutputStringStreamNullDereference};
		}
		*legacy_output_string_stream_ptr << hw.get_string_data() << std::endl;
		return ErrorCode{ErrorCode::NoError};
	}

  protected:
	std::ostream* legacy_output_string_stream_ptr;
};

struct HelloWorldOutputStreamBase {
	virtual std::ostream* get_stream() {return nullptr;};
	virtual ~HelloWorldOutputStreamBase() {}
};

class PrintStrategyTerminalOutput final: virtual public PrintStrategyLegacy {
  public:
	PrintStrategyTerminalOutput(HelloWorldOutputStreamBase &s): PrintStrategyBase(IntrusiveLoggerSingleton::get()), PrintStrategyLegacy(), stream(s) {}

	ErrorCode __do_print_job(HelloWorldStringBase const &hw) override {
		legacy_output_string_stream_ptr = stream.get_stream();
		return PrintStrategyLegacy::__do_print_job(hw);
	}

  private:
	HelloWorldOutputStreamBase &stream;
};

struct HelloWorldTerminalOutputStream final: virtual public HelloWorldOutputStreamBase {
	std::ostream* get_stream() override {
		return &std::cout;
	}
};

struct HelloWorldStringStringBased: public virtual HelloWorldStringBase {
	std::string data;
	char const * get_string_data() const override {return data.c_str();}
};

int main () {
	HelloWorldTerminalOutputStream terminal_output_stream;
	PrintStrategyTerminalOutput terminal_strategy (terminal_output_stream);
	HelloWorldStringStringBased hwstring;
	hwstring.data = "Hello, world!";
	
	auto code = terminal_strategy.setup_strategy();
	if (code) throw IntrusiveLoggerSingleton::get().error_message;
	code = terminal_strategy.do_print_job(hwstring);
	if (code) throw IntrusiveLoggerSingleton::get().error_message;

	IntrusiveLoggerSingleton::finish();

	return 0;
}